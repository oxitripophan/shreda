# shred

folder shredder written in c

to install:
	
	git clone https://codeberg.org/oxitripophan/shreda.git

	cd shreda

	make
	
	make clean install

to uninstall:

	make clean uninstall

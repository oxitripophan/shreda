PREFIX := /usr/local

final:
	gcc -lreadline -o shreda shreda.c 

clean:
	rm -f *.out

install: shreda
	install -D shreda $(DESTDIR)$(PREFIX)/bin/shreda

uninstall: shreda
	rm -rf /usr/local/bin/shreda

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>

char dir[200];
int cd_dir;
int shred_dir;

int get_dir(){
	char *input = readline("The directory you want to shred: ");
	if (strlen(input) > 0) {
		add_history(input);
		strcpy(dir,input);
		free(input);
		return 0;
	} else {
		printf("Error: no directory entered.\n");
		exit(1);
	}
}

int shred(){
	char *cds_dir = malloc(strlen(dir) + 100);
	sprintf(cds_dir, "cd %s && find . -maxdepth 1 -type f -print0 | xargs -0 shred -fuzv -n 27", dir);
	system(cds_dir);
	if (strlen(cds_dir) == 0) {
		printf("Error encountered while shredding files. \n");
		exit(1);
	}
	free(cds_dir);
	return 0;
}


int main(void){
	get_dir();
	shred();
	printf("folder shredded. \n");
	return 0;
}

